;https://sqlite.org/fts3.html
(define-module (clementine-fix)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages music)
  #:use-module (srfi srfi-1)
 )

(define sqlite-with-fts3-fixed
  (package (inherit sqlite-with-fts3)
    (arguments
     `( ,@(substitute-keyword-arguments (package-arguments sqlite-with-fts3)
                                      ((#:configure-flags cf)
                                        `(cons "CPPFLAGS=-DSQLITE_ENABLE_FTS3 -DSQLITE_ENABLE_FTS3_PARENTHESIS -DSQLITE_ENABLE_FTS3_TOKENIZER" ,cf)))))
    ))

(define-public clementine-fixed
  (package (inherit clementine)
    (inputs
      `(,@(alist-delete "sqlite" (package-inputs clementine))
         ("sqlite" ,sqlite-with-fts3-fixed)))
    ))
