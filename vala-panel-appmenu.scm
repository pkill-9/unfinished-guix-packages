(define-module (vala-panel-appmenu)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public vala-panel-appmenu
(package
  (name "vala-panel-appmenu")
  (version "0.6.94")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://github.com/rilian-la-te/vala-panel-appmenu/releases/download/"
             version
             "/vala-panel-appmenu-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "1akfhbmn7phbns5065xb0vq2smxwl4p9clcr8q7xg11jsxzacx24"))))
  (build-system cmake-build-system)
  (native-inputs
    `(("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("glib:bin" ,(@ (gnu packages glib) glib) "bin")
      ("gettext"
       ,(@ (gnu packages gettext) gnu-gettext))))
  (inputs
    `(("vala" ,(@ (gnu packages gnome) vala))
      ("xfce4-panel" ,(@ (gnu packages xfce) xfce4-panel))
      ("xfconf" ,(@ (gnu packages xfce) xfconf))
      ("libwnck" ,(@ (gnu packages gnome) libwnck)) ;This isn't required, but configure phase looks for it, so must add something interesting
      ("glib" ,(@ (gnu packages glib) glib))
      ("bamf" ,(@ (plank bamf) bamf))
      ("gtk+" ,(@ (gnu packages gtk) gtk+))))
    (arguments
     `(;#:tests? #f
       #:configure-flags '("-DENABLE_XFCE=ON" "-DENABLE_APPMENU_GTK_MODULE=ON")
       #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'specify-gtk-modules-dir
             (lambda _
	       (substitute* "subprojects/appmenu-gtk-module/src/CMakeLists.txt"
		(("set\\(GTK3_MODULEDIR .*") (string-append "set(GTK3_MODULEDIR " (assoc-ref %outputs "out") "/lib/gtk-3.0/modules)" "\n"))))))
       ))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))
 )

(package (inherit vala-panel-appmenu))
