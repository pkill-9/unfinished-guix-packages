(define-module (doomseeker)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public doomseeker
(package
  (name "doomseeker")
  (version "1.1")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://bitbucket.org/Doomseeker/doomseeker/get/"
             version
             ".tar.bz2"))
      (sha256
        (base32
          "1wjy6c197falag0jbvspld3pnil4avh81l6crgdbcg9hwnm2q2zh"))))
  (build-system cmake-build-system)
  (inputs
    `(("qt" ,(@ (gnu packages qt) qt))
      ("bzip2" ,(@ (gnu packages compression) bzip2))
      ("zlib" ,(@ (gnu packages compression) zlib))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
(package (inherit doomseeker))
